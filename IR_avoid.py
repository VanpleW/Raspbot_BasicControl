# left, right = IR_avoid()
def IR_avoid():
    import RPi.GPIO as GPIO
    import time
    GPIO.setmode(GPIO.BOARD)
    GPIO.setwarnings(False) 
    
    IR_avoid_left = 21
    IR_avoid_right = 19
    IR_avoid_switch = 22
    
    GPIO.setup(IR_avoid_left, GPIO.IN)
    GPIO.setup(IR_avoid_right, GPIO.IN)
    GPIO.setup(IR_avoid_switch, GPIO.OUT)
    GPIO.output(IR_avoid_switch, GPIO.HIGH)
    
    time.sleep(0.1)
    return IR_avoid_left, IR_avoid_right
