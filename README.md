The main source of all hardware/sensors are avaiable to called from:
> **raspbot.py**

The FPS test of the picamera is on:
> **FPS_test.py**

This repository contains the following baisc control-commands of a raspberry-pi-4-drived PCB car:
1. Motor.py to drive the car forward, backward, spin to left and spin to right.
2. Servo.py to control the upper and lower servo to adjsut the facing-direction of imaging sensor.
3. buzzer.py to have a beep sound.
4. Ultrasound.py to get the distance between the front object and the car itself.
5. IR_avoid.py to get if there is any object in front of the IR sensors.
6. LED.py to control the red and blue led.
7. LineTracking.py to get the returned status of four line sensors.
8. piVideoStream to provdie an OpenCV-based picamera streaming class.

There will be more functional-sensors to add on this repository.
including but not limited to: 
>  1. water sensor;
>  2. speaker;
>  3. OLED Screen;
>  ... ...

