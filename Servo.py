def servo(degree1, degree2):
	from YB_Pcb_Car import Pcb_Car
	import time
	car = Pcb_Car()
	
	car.Ctrl_Servo(1, degree1)
	time.sleep(0.5)
	
	car.Ctrl_Servo(2, degree2)
	time.sleep(0.5)
	
	del car

# ~ upperServo = 45
# ~ lowerServo = 78
# ~ Servo(0,0)
