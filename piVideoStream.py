from picamera.array import PiRGBArray
from picamera import PiCamera
from threading import Thread
import cv2

# parameters for camera configuration 
"""
    #define CV_CAP_PROP_POS_MSEC       0  // Calculate the current position in milliseconds

    #define CV_CAP_PROP_POS_FRAMES     1  // Calculate the current position in frame

    #define CV_CAP_PROP_POS_AVI_RATIO  2  // Relative position of the video

    #define CV_CAP_PROP_FRAME_WIDTH    3  // Width

    #define CV_CAP_PROP_FRAME_HEIGHT   4  // Height

    #define CV_CAP_PROP_FPS            5  // Frame rate

    #define CV_CAP_PROP_FOURCC         6  // 4 Character encoding

    #define CV_CAP_PROP_FRAME_COUNT    7  // Video frames

    #define CV_CAP_PROP_FORMAT         8  // Video format

    #define CV_CAP_PROP_MODE           9  // Backend specific value indicating the current capture mode.

    #define CV_CAP_PROP_BRIGHTNESS    10  // Brightness

    #define CV_CAP_PROP_CONTRAST      11  // Contrast

    #define CV_CAP_PROP_SATURATION    12  // Saturation

    #define CV_CAP_PROP_HUE           13  // Hue

    #define CV_CAP_PROP_GAIN          14  // Gain

    #define CV_CAP_PROP_EXPOSURE      15  // Exposure

    #define CV_CAP_PROP_CONVERT_RGB   16  // Mark whether the image should be converted to RGB.

    #define CV_CAP_PROP_WHITE_BALANCE 17 // White balance

    #define CV_CAP_PROP_RECTIFICATION 18 // Stereo camera calibration mark (note: only support DC1394 v2)
"""

class PiVideoStream:
	def __init__(self, resolution=(320,240), framerate = 32):
		# initialize the camera
		self.camera = PiCamera()
		self.camera.resolution = resolution
		self.camera.framerate = framerate
		self.rawCapture = PiRGBArray(self.camera, size=resolution)
		self.stream = self.camera.capture_continuous(self.rawCapture,format="bgr", use_video_port=True)
		
		# initialize the frames
		self.frame = None
		self.stopped = False
		
	def start(self):
		# start the thread to read frames
		Thread(target=self.update, args=()).start()
		return self
		
	def update(self):
		# grab the most recent frames and stores them
		for f in self.stream:
			self.frame = f.array
			self.rawCapture.truncate(0)
			# stop the thread and the resource camera
			if self.stopped:
				self.stream.close()
				self.rawCapture.close()
				self.camera.close()
				return
	
	def read(self):
		return self.frame
		
	def stop(self):
		self.stopped = True
