def water():
    from YB_Pcb_Car import Pcb_Car
    import time
    car = Pcb_Car()
	
    # water_level = int.from_bytes(car.read_Water(32), byteorder='big', signed=False)
    water_level = car.read_Water(4)
    time.sleep(0.5)
    
    del car
    return water_level

while True:
    print('water level', water() )
