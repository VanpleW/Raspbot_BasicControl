def led(red_switch, blue_switch):
    
    import RPi.GPIO as GPIO
    import time
    GPIO.setmode(GPIO.BOARD)
    GPIO.setwarnings(False)
    
    LED_red = 40
    LED_blue = 38
    GPIO.setup(LED_red, GPIO.OUT)
    GPIO.setup(LED_blue, GPIO.OUT)
    
    if red_switch:
        GPIO.output(LED_red, GPIO.HIGH)
    else:
        GPIO.output(LED_red, GPIO.LOW)
    
    if blue_switch:
        GPIO.output(LED_blue, GPIO.HIGH)
    else:
        GPIO.output(LED_blue, GPIO.LOW)
    
    print('LED settings are completed')
    

