def getDistance():
	
	import RPi.GPIO as GPIO
	import time
	from statistics import mean
	GPIO.setwarnings(False)
	
	# define pins of ultrasonic module
	EchoPin = 18
	TrigPin = 16
	# set the pin mode
	GPIO.setmode(GPIO.BOARD)
	GPIO.setup(EchoPin, GPIO.IN)
	GPIO.setup(TrigPin, GPIO.OUT)
	
	def Distance():
		# send a 15us high pulse to trigger the ultrasound sensor
		GPIO.output(TrigPin, GPIO.LOW)
		time.sleep(0.000002)
		GPIO.output(TrigPin, GPIO.HIGH)
		time.sleep(0.000015)
		GPIO.output(TrigPin, GPIO.LOW)
		
		# record time for pulse to travel
		t_start = time.time()
		t_stop = time.time()
		t_ref1 = time.time()
		while not GPIO.input(EchoPin):
			t_start = time.time()
			if t_start - t_ref1 > 0.03:
				return -1
		t_ref2 = time.time()		
		while GPIO.input(EchoPin):
			t_stop = time.time()
			if t_stop - t_ref2 > 0.03:
				return -1
		time.sleep(0.01)
		# output distance as cm
		return ((t_stop - t_start)*340/2) * 100
	
	temp = []
	for i in range (5):
		temp.append(Distance())
		time.sleep(0.01)
	dist = mean(temp)
	if abs(dist - Distance()) < 5:
		return dist


# ~ import time
# ~ while True:	
	# ~ print('Distance', ultrasound())
	# ~ time.sleep(1)
