from buzzer import beep
from Motor import forward, backward, spin_left, spin_right
from LED import led
from LineTracking import tracking
from Servo import servo
from Ultrasound import getDistance
from IR_avoid import IR_avoid

def bot_beep():
	beep()

def bot_forward(left,right,time):
	forward(left, right, time)
	
def bot_backward(left,right,time):
	backward(left,right,time)
	
def bot_spin_left(left,right,time):
	spin_left(left,right,time)
	
def bot_spin_right(left,right,time):
	spin_right(left,right,time)
	
def bot_led(red, blue):
	led(red, blue)
	
def bot_tracking():
	return tracking()

def bot_servo(vert, hori):
	servo(vert, hori)
	
def bot_getDistance():
	return getDistance()

def bot_irAvoid():
	return IR_avoid()
