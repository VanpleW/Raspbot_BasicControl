# return an array of four elements
def tracking():
    import RPi.GPIO as GPIO
    import time
    GPIO.setmode(GPIO.BOARD)
    GPIO.setwarnings(False) 
    
    tracking_left = 13
    tracking_mid_left = 15
    tracking_mid_right = 11
    tracking_right = 7
    
    GPIO.setup(tracking_left, GPIO.IN)
    GPIO.setup(tracking_mid_left, GPIO.IN)
    GPIO.setup(tracking_mid_right, GPIO.IN)
    GPIO.setup(tracking_right, GPIO.IN)
    
    time.sleep(0.1)
    tracking_matrix = [tracking_left, tracking_mid_left, tracking_mid_right, tracking_right]
    return tracking_matrix