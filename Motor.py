def forward(left_speed,right_speed,duration):
	from YB_Pcb_Car import Pcb_Car
	import time
	
	car = Pcb_Car()
	car.Car_Run(left_speed, right_speed)
	time.sleep(duration)
	car.Car_Stop()
	del car

def spin_right(left_speed,right_speed,duration):
	from YB_Pcb_Car import Pcb_Car
	import time
	
	car = Pcb_Car()
	car.Car_Spin_Right(left_speed, right_speed)
	time.sleep(duration)
	car.Car_Stop()
	del car
	
def spin_left(left_speed,right_speed,duration):
	from YB_Pcb_Car import Pcb_Car
	import time
	
	car = Pcb_Car()
	car.Car_Spin_Left(left_speed, right_speed)
	time.sleep(duration)
	car.Car_Stop()
	del car

def backward(left_speed,right_speed,duration):
	from YB_Pcb_Car import Pcb_Car
	import time
	
	car = Pcb_Car()
	car.Car_Back(left_speed, right_speed)
	time.sleep(duration)
	car.Car_Stop()
	del car

# ~ forward(50,50,1)
# ~ spin_left(100,100,1)
